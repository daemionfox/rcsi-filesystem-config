<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 2/13/15
 * Time: 10:49 PM
 */

namespace RCSI\Config;


use RCSI\Exceptions\ConfigException;

/**
 * Class Config
 * @package RCSI\Config
 */
class Config {

    /** Singleton instance
     * @var Config
     */
    protected static $instance;

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /** Path to config directory
     * @var string
     */
    protected $baseDir;

    /** Config file name
     * @var string
     */

    protected $file = "config.ini";

    /** Contains the config file data
     * @var array
     */
    protected $config = array();

    /** Constructor
     *
     */
    public function __construct()
    {
        $this->baseDir = __DIR__ . "/config";
    }

    /** Set the config directory
     * @param string $dir
     * @return $this
     * @throws ConfigException
     */
    public function setDir($dir)
    {
        if (!is_dir($dir)) {
            throw new ConfigException("Could not locate config directory");
        }
        $this->baseDir = $dir;
        return $this;
    }

    /** Generate the singleton
     * @return Config
     */
    public static function init()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /** Get a config value
     * @param $key
     * @return null
     */
    public function get($key, $default = null)
    {
        if (isset($this->config[$key])) {
            return $this->config[$key];
        }
        return $default;
    }

    /** Get the config value and force it to return a boolean
     * @param $key
     * @return bool
     */
    public function getBool($key)
    {
        if (
            isset($this->config[$key]) && (
                strtolower($this->config[$key]) === true ||
                intval($this->config[$key]) === 1
            )
        ){
            return true;
        }
        return false;
    }

    /**
     * Returns an array of values from the config file splitting on a given delimiter (default comma)
     * @param $key
     * @param string $delim
     * @return array|null
     */
    public function getArray($key, $delim = ',')
    {
        if (empty($this->config[$key])) {
            return array();
        }
        $string = $this->config[$key];
        $array = explode($delim, $string);
        $array = array_map("trim", $array);
        return $array;
    }

    /** Set the config file name
     * @param string $file
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /** Return the config directory
     *
     * @return string
     */
    public function getDir()
    {
        return $this->baseDir;
    }

    /** Read the config file and load the data
     *
     * @param string|null $type
     * @return $this
     * @throws ConfigException
     */
    public function load($type = null)
    {
        if(!file_exists($this->baseDir . "/" . $this->file)){
            throw new ConfigException("Config file not found: " . $this->baseDir . "/" . $this->file);
        }

        $fType = $type === null ? $this->determineFileType() : $type;

        switch ($fType) {
            case "INI":
                $this->loadINI();
                break;
            case "JSON":
                $this->loadJSON();
                break;
            default:
                throw new ConfigException("Could not determine the file type");
                break;
        }
        return $this;
    }

    /** Save the configuration
     * @param string|null $type
     * @return $this
     * @throws ConfigException
     */
    public function save($type = null)
    {
        $fType = $type === null ? $this->determineFileType() : $type;

        switch ($fType) {
            case "INI":
                $this->saveINI();
                break;
            case "JSON":
                $this->saveJSON();
                break;
            default:
                throw new ConfigException("Could not save configuration");
                break;
        }
        return $this;
    }

    /** Write the config to ini
     *
     * @return $this
     * @throws ConfigException
     */
    protected function saveINI()
    {
        $out = array();
        foreach($this->config as $key => $value) {
            $out[] = "{$key}=$value";
        }

        try {
            file_put_contents($this->baseDir . "/" . $this->file, implode("\n", $out));
        } catch (\Exception $e) {
            throw new ConfigException("Could not save ini file");
        }

        return $this;
    }

    /** Write the config to json
     *
     * @return $this
     * @throws ConfigException
     */
    protected function saveJSON()
    {

        try {
            file_put_contents($this->baseDir . "/" . $this->file, json_encode($this->config));
        } catch (\Exception $e) {
            throw new ConfigException("Could not save json file");
        }

        return $this;
    }

    /** Read the file name and determine the file type from the extension
     *
     * @return string
     * @throws ConfigException
     */
    public function determineFileType()
    {
        $type = strtoupper(pathinfo($this->baseDir . "/" . $this->file, PATHINFO_EXTENSION));
        return $type;
    }

    /** Read INI Files
     *
     * @return $this
     */
    protected function loadINI()
    {
        $this->config = parse_ini_file($this->baseDir . "/{$this->file}");
        $pattern = '/(\%\{[A-Za-z0-9]+\})/';
        foreach ($this->config as $key => $value) {
            $match = preg_match_all($pattern, $value, $matches);
            if ($match) {
                foreach($matches[1] as $m) {
                    $newKey = trim($m, "%\{\}");
                    $newVal = $this->get($newKey);
                    $value = str_replace($m, $newVal, $value);
                }
            }
            $this->config[$key] = $value;
        }
        return $this;
    }

    /** Read JSON Files
     * @return $this
     */
    protected function loadJSON()
    {
        $this->config = json_decode(
            file_get_contents($this->baseDir . "/{$this->file}"),
            true
        );
        return $this;
    }

    /** Returns a listing of all keys in the config
     * @return array
     */
    public function getKeys()
    {
        $keys = array_keys($this->config);
        return $keys;
    }

    public function set($key, $value)
    {
        $this->config[$key] = $value;
        return $this;
    }

    public function addArray($key, $value)
    {
        if (isset($this->config[$key])) {
            if (is_array($this->config[$key])) {
                $this->config[$key][] = $value;
                return $this;
            }
            throw new ConfigException("{$key} is not an array");
        }
        $this->config[$key] = array(
            $value
        );
        return $this;

    }

    public function dump()
    {
        return $this->config;
    }

}
