<?php
/**
 * Created by PhpStorm.
 * User: msowers
 * Date: 12/3/15
 * Time: 10:29 AM
 */

namespace RCSI\Wrapper;

use RCSI\Config\Config;
use RCSI\Exceptions\ConfigException;

class ConfigWrapper
{

    protected static $config;
    protected static $path = null;

    /**
     * Initialize the config
     *
     * @return Config
     * @throws \RCSI\Exceptions\ConfigException
     */
    public static function init()
    {
        if(self::$config === null) {
            $me = new self();
            $me->determineConfigPath();
            $config = Config::init();
            $config->setDir(self::$path);
            $config->load();
            self::$config = $config;
        }

        return self::$config;
    }




    protected function determineConfigPath()
    {
        /**
         * We need to locate the config dir.  Making two assumptions:

         * 1) We've already set the directory somewhere else, and can escape quickly.

         * 2) We are in a vendor directory from Composer, in which case
         * we're pretty deep down.  We can guess the approximate depth, and go from there.
         *
         * 3) We're not in a vendor directory, we can try something shallower.
         *
         */

        if (self::$path !== null && is_dir(self::$path)){
            return self::$path;
        }

        // Assume that this belongs to a vendor directory...
        $testDir = __DIR__ . "/../../../../../config";
        if (file_exists($testDir) && is_dir($testDir)) {
            self::$path = realpath($testDir);
            return self::$path;
        }

        $testDir = __DIR__ . "/../../config";
        if (file_exists($testDir) && is_dir($testDir)) {
            self::$path = realpath($testDir);
            return self::$path;
        }

        throw new ConfigException("Could not determine path for config");
    }

    public static function setPath($path)
    {
        self::$path = $path;
        return self::$path;
    }
}
