# Filesystem Config

> rcsi/filesystem-config is an easy to use configuration tool that uses ini or json files for system configuration

## About

This tool provides a simple interface for setting and retrieving configuration variables

## Composer

add 'rcsi/filesystem-config' to your composer requirements

## License

MIT License - https://opensource.org/licenses/MIT

## Documentation

This class will generate a php singleton which all of the variables in a config.ini or config.json file have been loaded.

The code is documented inline, and should be clear with a little reading.

### Methods

**Config::init()**
Returns a config singleton

**get(string $key, mixed|null $default = null)**
Returns the value of $key or $default.  If no default is provided, returns null

**getBool(string $key)**
Returns a boolean based on the key.  The strings 'true' or '1' will return true, all else returns false

**getArray(string $key, string $delim = ',')**
Returns an array split along $delim.  If no delimiter is provided, it will default to a ','

**setFile(string $file)**
Sets the file name for config

**getDir()**
Returns the config directory

**load()**
Loads the config file into the singleton

**save(string|null $type = null)**
Saves the singleton to the path/file with appropriate attempts to discover file type if not provided

**saveINI()**
Saves the singleton as an INI file to the path

**saveJSON()**
Saves the singleton as a JSON file to the path

**determineFileType()**
Reads the extension of the file in order to detect it's file type

**getKeys()**
Returns all available keys in the config 

**set($key, $value)**
Sets a config variable

**addArray($key, $value)**
Adds a value to an config array 

**dump()**
Dumps the config

## Wrapper

Filesystem-config contains ConfigWrapper, which will automatically attempt to detect your configuration path and provide a loaded config singleton,
Or, if you prefer, you can specifiy the config path to bypass discovery.  By default, it looks for a config/ directory at the same level as your vendor/ directory.

### Useage
```
ConfigWrapper::setPath(__DIR__ . "/../config"); // Optional...
$config = ConfigWrapper::init()
```

## Example

### Contents of config.ini

```
[Examples]
stringOne = This is a string
boolOne = 0
boolTwo = 1
boolThree = true
arrayOne = One, Two, Three, Four
```


### Code
```
$config = ConfigWrapper::init();
$string = $config->get('stringOne');              // Returns 'This is a string'
$string = $config->get('stringTwo');              // Returns null
$string = $config->get('stringTwo', 'default');   // Returns 'default'
$bool   = $config->get('boolOne');                // Returns '0'
$bool   = $config->getBool('boolOne');            // Returns false
$bool   = $config->getBool('boolTwo');            // Returns true
$bool   = $config->getBool('boolThree');          // Returns true
$bool   = $config->getBool('boolFour');           // Returns false
$array  = $config->getArray('arrayOne');          // Returns ['One', 'Two', 'Three', 'Four']
```